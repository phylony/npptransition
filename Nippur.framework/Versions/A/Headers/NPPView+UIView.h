/*
 *	NPPView+UIView.h
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 9/27/11.
 *	Copyright 2011 db-in. All rights reserved.
 */

#import "NPPRuntime.h"
#import "NPPFunctions.h"

#pragma mark -
#pragma mark UIView Category
//**************************************************
//	UIView Category
//**************************************************

@interface UIView(NPPView)

//*************************
//	Graphics
//*************************
- (UIImage *) snapshot;
- (UIImage *) snapshotInRect:(CGRect)rect;
+ (UIImage *) snapshotForView:(UIView *)view inRect:(CGRect)rect;

@end

#pragma mark -
#pragma mark CALayer Category
//**************************************************
//	CALayer Category
//**************************************************

@interface CALayer(NPPLayer)

//*************************
//	Basics
//*************************
- (CGPoint) pivot;
- (void) setPivot:(CGPoint)value;

@end