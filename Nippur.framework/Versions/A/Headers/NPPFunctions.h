/*
 *	NPPFunctions.h
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 8/15/12.
 *	Copyright 2012 db-in. All rights reserved.
 */

#import "NPPRuntime.h"

/*!
 *					Single void block definition. This is the first convention of blocks to make the coding
 *					cleaner and faster.
 */
typedef void (^NPPBlockVoid)(void);

/*!
 *					Returns the current version of the running OS. The result should be compared to
 *					a NPP_IOS_X_X definition.
 *
 *	@result			A float with the version of the running OS.
 */
NPP_API float nppDeviceSystemVersion(void);

/*!
 *					Returns the current version of the running OS. The result should be compared to
 *					a NPP_IOS_X_X definition.
 *
 *	@param			aClass
 *					The class that will have its method swizzled.
 *
 *	@param			older
 *					The original selector, that means, the one which will no longer respond to its call.
 *
 *	@param			newer
 *					The original selector, that means, the one which will no longer respond to its call.
 */
NPP_API void nppSwizzle(Class aClass, SEL older, SEL newer);