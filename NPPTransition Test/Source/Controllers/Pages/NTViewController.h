/*
 *	NTViewController.h
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 6/9/13.
 *	Copyright 2013 db-in. All rights reserved.
 */

#import "UIColor+NTColor.h"
#import "NTNotificationView.h"

@interface NTViewController : UIViewController
{
@private
	
}

@property (nonatomic, NPP_ASSIGN) IBOutlet UIButton *btNext;
@property (nonatomic, NPP_ASSIGN) IBOutlet UIButton *btModal;
@property (nonatomic, NPP_ASSIGN) IBOutlet UIButton *btNotification;

- (IBAction) actionNext:(id)sender;
- (IBAction) actionModal:(id)sender;
- (IBAction) actionNotification:(id)sender;

@end