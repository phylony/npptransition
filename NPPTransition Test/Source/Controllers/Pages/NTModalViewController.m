/*
 *	NTModalViewController.m
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 6/9/13.
 *	Copyright 2013 db-in. All rights reserved.
 */

#import "NTModalViewController.h"

#pragma mark -
#pragma mark Constants
#pragma mark -
//**********************************************************************************************************
//
//	Constants
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Interface
#pragma mark -
//**********************************************************************************************************
//
//	Private Interface
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Definitions
//**************************************************
//	Private Definitions
//**************************************************

#pragma mark -
#pragma mark Private Functions
//**************************************************
//	Private Functions
//**************************************************

#pragma mark -
#pragma mark Private Category
//**************************************************
//	Private Category
//**************************************************

#pragma mark -
#pragma mark Public Interface
#pragma mark -
//**********************************************************************************************************
//
//	Public Interface
//
//**********************************************************************************************************

@implementation NTModalViewController

#pragma mark -
#pragma mark Properties
//**************************************************
//	Properties
//**************************************************

@synthesize btSegue = _btSegue, btDismiss = _btDismiss;

#pragma mark -
#pragma mark Constructors
//**************************************************
//	Constructors
//**************************************************

#pragma mark -
#pragma mark Private Methods
//**************************************************
//	Private Methods
//**************************************************

#pragma mark -
#pragma mark Self Public Methods
//**************************************************
//	Self Public Methods
//**************************************************

- (IBAction) actionDismiss:(id)sender
{
	[[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (void) viewDidLoad
{
	[super viewDidLoad];
	
	// Chooses the next view color.
	self.view.backgroundColor = [UIColor nextViewColor];
	
	CALayer *layer;
	layer = _btSegue.layer;
	layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
	layer.shadowOpacity = 0.15f;
	layer.shadowRadius = 0.0f;
	layer.shadowColor = [[UIColor blackColor] CGColor];
	
	layer = _btDismiss.layer;
	layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
	layer.shadowOpacity = 0.15f;
	layer.shadowRadius = 0.0f;
	layer.shadowColor = [[UIColor blackColor] CGColor];
}

- (void) dealloc
{
#ifndef NPP_ARC
	[super dealloc];
#endif
}

@end
