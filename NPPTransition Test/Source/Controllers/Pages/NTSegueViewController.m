/*
 *	NTSegueViewController.m
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 6/9/13.
 *	Copyright 2013 db-in. All rights reserved.
 */

#import "NTSegueViewController.h"

#pragma mark -
#pragma mark Constants
#pragma mark -
//**********************************************************************************************************
//
//	Constants
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Interface
#pragma mark -
//**********************************************************************************************************
//
//	Private Interface
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Definitions
//**************************************************
//	Private Definitions
//**************************************************

#pragma mark -
#pragma mark Private Functions
//**************************************************
//	Private Functions
//**************************************************

#pragma mark -
#pragma mark Private Category
//**************************************************
//	Private Category
//**************************************************

#pragma mark -
#pragma mark Public Interface
#pragma mark -
//**********************************************************************************************************
//
//	Public Interface
//
//**********************************************************************************************************

@implementation NTSegueViewController

#pragma mark -
#pragma mark Properties
//**************************************************
//	Properties
//**************************************************

@synthesize btBack = _btBack, tfExample = _tfExample, lbExample = _lbExample,
			hsExample = _hsExample, swExample = _swExample;

#pragma mark -
#pragma mark Constructors
//**************************************************
//	Constructors
//**************************************************

#pragma mark -
#pragma mark Private Methods
//**************************************************
//	Private Methods
//**************************************************

- (void) animate
{
	[_flip perform];
	[_flip performBackwardAfterDelay:2.0f];
	
	[_easing perform];
	[_easing performBackwardAfterDelay:2.0f];
	
	[self performSelector:_cmd withObject:nil afterDelay:4.0f];
}

#pragma mark -
#pragma mark Self Public Methods
//**************************************************
//	Self Public Methods
//**************************************************

- (IBAction) actionDismiss:(id)sender
{
	NPPTransitionFold *fold = [NPPTransitionFold transitionWithcompletion:nil];
	[[self presentingViewController] nppDismissViewControllerAnimated:YES transition:fold];
}

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (void) viewDidLoad
{
	[super viewDidLoad];
	
	// Chooses the next view color.
	self.view.backgroundColor = [UIColor nextViewColor];
	
	CALayer *layer;
	layer = _btBack.layer;
	layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
	layer.shadowOpacity = 0.15f;
	layer.shadowRadius = 0.0f;
	layer.shadowColor = [[UIColor blackColor] CGColor];
}

- (void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	_flip = [[NPPTransitionFlip alloc] initFromView:_lbExample toView:_tfExample completion:nil];
	_flip.direction = NPPDirectionDown;
	_flip.mode = NPPTransitionModeShowHide;
	
	_easing = [[NPPTransitionEasing alloc] initFromView:_hsExample toView:_swExample completion:nil];
	_easing.direction = NPPDirectionRight;
	_easing.mode = NPPTransitionModeShowHide;
	
	[self animate];
}

- (void) viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
	
	[NSObject cancelPreviousPerformRequestsWithTarget:self];
	
	[_flip cancelPerformBackward];
	[_easing cancelPerformBackward];
	nppRelease(_flip);
	nppRelease(_easing);
}

- (void) dealloc
{
#ifndef NPP_ARC
	[super dealloc];
#endif
}

@end
