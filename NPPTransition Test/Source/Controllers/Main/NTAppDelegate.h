//
//  NTAppDelegate.h
//  Nippur Test
//
//  Created by Diney Bomfim on 2/25/13.
//  Copyright (c) 2013 db-in. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NTAppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, NPP_RETAIN) UIWindow *window;

@end
