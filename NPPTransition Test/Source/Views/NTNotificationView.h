/*
 *	NTNotificationView.h
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 6/9/13.
 *	Copyright 2013 db-in. All rights reserved.
 */

#import "UIColor+NTColor.h"
#import "UIImage+NTImage.h"

typedef enum
{
	NTNotificationTypeiOS6,
	NTNotificationTypeiOS7,
} NTNotificationType;

@interface NTNotificationView : UIView
{
@private
	UILabel			*_title;
	UILabel			*_subTitle;
	UIImageView		*_icon;
}

@property (nonatomic, NPP_RETAIN) NSString *titleText;
@property (nonatomic, NPP_RETAIN) NSString *subText;
@property (nonatomic, NPP_RETAIN) UIImage *icon;

- (id) initWithType:(NTNotificationType)type;
+ (id) notificationWithType:(NTNotificationType)type;

@end