/*
 *	NTNotificationView.m
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 6/9/13.
 *	Copyright 2013 db-in. All rights reserved.
 */

#import "NTNotificationView.h"

#pragma mark -
#pragma mark Constants
#pragma mark -
//**********************************************************************************************************
//
//	Constants
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Interface
#pragma mark -
//**********************************************************************************************************
//
//	Private Interface
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Definitions
//**************************************************
//	Private Definitions
//**************************************************

#pragma mark -
#pragma mark Private Functions
//**************************************************
//	Private Functions
//**************************************************

#pragma mark -
#pragma mark Private Category
//**************************************************
//	Private Category
//**************************************************

@interface NTNotificationView()

- (void) initializing;

@end

#pragma mark -
#pragma mark Public Interface
#pragma mark -
//**********************************************************************************************************
//
//	Public Interface
//
//**********************************************************************************************************

@implementation NTNotificationView

#pragma mark -
#pragma mark Properties
//**************************************************
//	Properties
//**************************************************

@dynamic titleText, subText, icon;

- (NSString *) titleText { return _title.text; }
- (void) setTitleText:(NSString *)value
{
	_title.text = value;
}

- (NSString *) subText { return _subTitle.text; }
- (void) setSubText:(NSString *)value
{
	_subTitle.text = value;
}

- (UIImage *) icon { return _icon.image; }
- (void) setIcon:(UIImage *)value
{
	_icon.image = value;
}

#pragma mark -
#pragma mark Constructors
//**************************************************
//	Constructors
//**************************************************

- (id) initWithType:(NTNotificationType)type
{
	if ((self = [super initWithFrame:CGRectZero]))
	{
		[self initializing];
		
		CGRect frame;
		UIImage *bg;
		UIImageView *bgView;
		UIViewAutoresizing mask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		
		switch (type)
		{
			case NTNotificationTypeiOS7:
				frame = self.frame;
				frame.size.height = 40.0f;
				
				self.frame = frame;
				self.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.8f];
				_title.textColor = [UIColor whiteColor];
				_subTitle.textColor = [UIColor whiteColor];
				break;
			case NTNotificationTypeiOS6:
			default:
				frame = self.frame;
				frame.size.height = 40.0f;
				
				self.frame = frame;
				self.backgroundColor = [UIColor clearColor];
				_title.textColor = [UIColor colorWithHexadecimal:0x353535];
				_subTitle.textColor = [UIColor colorWithHexadecimal:0x353535];
				
				bg = [UIImage imageNamed:@"notification_ios6.png"];
				bgView = [[UIImageView alloc] initWithImage:[bg imageNineSliced]];
				bgView.contentMode = UIViewContentModeScaleToFill;
				bgView.autoresizingMask = mask;
				bgView.frame = self.frame;
				[self insertSubview:bgView atIndex:0];
				nppRelease(bgView);
				break;
		}
	}
	
	return self;
}

+ (id) notificationWithType:(NTNotificationType)type
{
	NTNotificationView *notification = [[NTNotificationView alloc] initWithType:type];
	return nppAutorelease(notification);
}

#pragma mark -
#pragma mark Private Methods
//**************************************************
//	Private Methods
//**************************************************

- (void) initializing
{
	UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
	CGRect screen = [[UIScreen mainScreen] bounds];
	CGRect frame = CGRectZero;
	
	switch (orientation)
	{
		case UIInterfaceOrientationLandscapeLeft:
		case UIInterfaceOrientationLandscapeRight:
			frame.size.width = screen.size.height;
			break;
		case UIInterfaceOrientationPortrait:
		case UIInterfaceOrientationPortraitUpsideDown:
		default:
			frame.size.width = screen.size.width;
			break;
	}
	
	self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	self.frame = frame;
	
	UIImage *icon = [UIImage imageNamed:@"Icon-Small.png"];
	CGRect labelFrame = CGRectMake(50.0f, 5.0f, frame.size.width - 80.0f, 15.0f);
	CGRect iconFrame = CGRectMake(7.0f, 7.0f, 25.0f, 25.0f);
	
	_title = [[UILabel alloc] initWithFrame:labelFrame];
	_title.backgroundColor = [UIColor clearColor];
	_title.font = [UIFont boldSystemFontOfSize:14.0f];
	_title.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
	
	labelFrame.origin.y += labelFrame.size.height;
	_subTitle = [[UILabel alloc] initWithFrame:labelFrame];
	_subTitle.backgroundColor = [UIColor clearColor];
	_subTitle.font = [UIFont systemFontOfSize:12.0f];
	_subTitle.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
	
	_icon = [[UIImageView alloc] initWithImage:icon];
	_icon.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
	_icon.frame = iconFrame;
	_icon.layer.cornerRadius = 5.0f;
	_icon.layer.masksToBounds = YES;
	
	self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	[self addSubview:_title];
	[self addSubview:_subTitle];
	[self addSubview:_icon];
}

#pragma mark -
#pragma mark Self Public Methods
//**************************************************
//	Self Public Methods
//**************************************************

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (void) dealloc
{
	nppRelease(_title);
	nppRelease(_subTitle);
	nppRelease(_icon);
	
#ifndef NPP_ARC
	[super dealloc];
#endif
}

@end
