//
//  main.m
//  Nippur Test
//
//  Created by Diney Bomfim on 2/25/13.
//  Copyright (c) 2013 db-in. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NTAppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
		return UIApplicationMain(argc, argv, nil, NSStringFromClass([NTAppDelegate class]));
	}
}
