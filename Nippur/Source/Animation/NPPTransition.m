/*
 *	NPPTransition.m
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 6/1/13.
 *	Copyright 2013 db-in. All rights reserved.
 */

#import "NPPTransition.h"

#pragma mark -
#pragma mark Constants
#pragma mark -
//**********************************************************************************************************
//
//	Constants
//
//**********************************************************************************************************

#define NPP_ERROR_INVALID			@"NPPTransition Invalid Operation"
#define NPP_ANI_ERROR_SUPERVIEW		@"The \"from view\" must be a subview of other view."
#define NPP_ANI_ERROR_ABSTRACT		@"The %@ is an abstract class and can't be used directly."
#define NPP_ANI_ERROR_SUBLCASS		@"The %@ is not a valid subclass of NPPTransition."

#pragma mark -
#pragma mark Private Interface
#pragma mark -
//**********************************************************************************************************
//
//	Private Interface
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Definitions
//**************************************************
//	Private Definitions
//**************************************************

static CGFloat _nppDefaultDuration = 0.3f;

static NPPDirection _nppDefaultDirection = NPPDirectionLeft;

static Class _nppDefaultModalClass = NULL;

static NPPDirection _nppDefaultModalDirection = NPPDirectionUp;

static Class _nppDefaultPushClass = NULL;

static NPPDirection _nppDefaultPushDirection = NPPDirectionLeft;

#pragma mark -
#pragma mark Private Functions
//**************************************************
//	Private Functions
//**************************************************

static NSInteger nppTransitionGetTopIndex(UIView *fromView, UIView *toView)
{
	UIView *superView = [fromView superview];
	NSArray *subViews = [superView subviews];
	NSInteger index = [subViews indexOfObject:fromView];
	index = (superView == [toView superview]) ? MAX([subViews indexOfObject:toView], index) : index;
	
	return index + 1;
}

#pragma mark -
#pragma mark Private Category
//**************************************************
//	Private Category
//**************************************************

@interface NPPTransition()

- (void) changeViewsState:(BOOL)isOpening backward:(BOOL)isBackward container:(UIView *)container;

@end

#pragma mark -
#pragma mark Public Interface
#pragma mark -
//**********************************************************************************************************
//
//	Public Interface
//
//**********************************************************************************************************

@implementation NPPTransition

#pragma mark -
#pragma mark Properties
//**************************************************
//	Properties
//**************************************************

@dynamic fromView, toView, mode, direction, backward;

- (UIView *) fromView { return _fromView; }
- (void) setFromView:(UIView *)value
{
	// Silently ignores any change while animating.
	if (!_isAnimating && _fromView != value)
	{
		nppRelease(_fromView);
		_fromView = nppRetain(value);
	}
}

- (UIView *) toView { return _toView; }
- (void) setToView:(UIView *)value
{
	// Silently ignores any change while animating.
	if (!_isAnimating && _toView != value)
	{
		nppRelease(_toView);
		_toView = nppRetain(value);
	}
}

- (NPPTransitionMode) mode { return _mode; }
- (void) setMode:(NPPTransitionMode)value
{
	// Silently ignores any change while animating.
	if (!_isAnimating)
	{
		_mode = value;
	}
}

- (NPPDirection) direction { return _direction; }
- (void) setDirection:(NPPDirection)value
{
	// Silently ignores any change while animating.
	if (!_isAnimating)
	{
		_direction = value;
	}
}

- (BOOL) isBackward { return _isBackward; }
- (void) setBackward:(BOOL)value
{
	// Silently ignores any change while animating.
	if (!_isAnimating)
	{
		_isBackward = value;
	}
}

#pragma mark -
#pragma mark Constructors
//**************************************************
//	Constructors
//**************************************************

- (id) initFromView:(UIView *)fromView toView:(UIView *)toView completion:(NPPBlockVoid)block
{
	if ((self = [super initWithDuration:_nppDefaultDuration completion:block]))
	{
		self.fromView = fromView;
		self.toView = toView;
		self.direction = _nppDefaultDirection;
	}
	
	return self;
}

+ (id) transitionFromView:(UIView *)fromView toView:(UIView *)toView completion:(NPPBlockVoid)block
{
	NPPTransition *transition = [[self alloc] initFromView:fromView toView:toView completion:block];
	return nppAutorelease(transition);
}

+ (id) transitionWithcompletion:(NPPBlockVoid)block
{
	NPPTransition *transition = [[self alloc] initWithDuration:_nppDefaultDuration completion:block];
	return nppAutorelease(transition);
}

#pragma mark -
#pragma mark Private Methods
//**************************************************
//	Private Methods
//**************************************************

- (void) changeViewsState:(BOOL)isOpening backward:(BOOL)isBackward container:(UIView *)container
{
	UIView *currentView = (isBackward) ? _toView : _fromView;
	UIView *finalView = (isBackward) ? _fromView : _toView;
	
	// Opening.
	if (isOpening)
	{
		switch (_mode)
		{
			case NPPTransitionModeOverride:
				// Do nothing.
				break;
			case NPPTransitionModeShowHide:
				currentView.hidden = YES;
				finalView.hidden = YES;
				break;
			case NPPTransitionModeAddRemove:
				[currentView removeFromSuperview];
				[finalView removeFromSuperview];
				break;
			case NPPTransitionModeNavigate:
			default:
				// Do nothing.
				break;
		}
	}
	// Closing.
	else
	{
		switch (_mode)
		{
			case NPPTransitionModeOverride:
				if (isBackward)
				{
					[currentView removeFromSuperview];
				}
				else
				{
					[[currentView superview] addSubview:finalView];
				}
				break;
			case NPPTransitionModeShowHide:
				currentView.hidden = YES;
				finalView.hidden = NO;
				break;
			case NPPTransitionModeAddRemove:
				[currentView removeFromSuperview];
				[[container superview] addSubview:finalView];
				break;
			case NPPTransitionModeNavigate:
			default:
				break;
		}
	}
}

+ (void) defineViewFramesFrom:(UIViewController *)fromController to:(UIViewController *)toController
{
	UIView *fromView = fromController.view;
	CGRect toFrame = fromView.frame;
	CGRect frameToAdjust;
	CGRect frameInWindow;
	CGRect frameInView;
	CGRect frameConverted;
	
	// In case of Modal.
	// Present.
	if ([fromController wantsFullScreenLayout])
	{
		frameToAdjust = [[UIApplication sharedApplication] statusBarFrame];
		frameInWindow = [fromView.window convertRect:frameToAdjust fromWindow:nil];
		frameInView = [fromView convertRect:frameInWindow fromView:nil];
		frameConverted = [fromView convertRect:toFrame fromView:nil];
		toFrame = frameConverted;
		toFrame.origin.y += frameInView.size.height;
		toFrame.size.height -= frameInView.size.height;
	}
	
	// Dismiss.
	if ([toController wantsFullScreenLayout])
	{
		frameToAdjust = fromView.window.frame;
		frameInWindow = [fromView.window convertRect:frameToAdjust fromWindow:nil];
		frameInView = [fromView convertRect:frameInWindow fromView:nil];
		toFrame = frameInView;
		toFrame.origin.y = 0.0f;
		
		// TODO Calculates the new NavigationBar and ToolBar frames.
	}
	
	// Prepares the target view with its final frame.
	toController.view.frame = toFrame;
	
	// TODO [toController viewWillDisappear:YES];
	// TODO [toController viewWillAppear:YES];
}

#pragma mark -
#pragma mark Self Public Methods
//**************************************************
//	Self Public Methods
//**************************************************

- (void) performBackward
{
	[self cancelPerformBackward];
	
	BOOL currentBackward = _isBackward;
	_isBackward = YES;
	[self perform];
	_isBackward = currentBackward;
}

- (void) performBackwardAfterDelay:(NSTimeInterval)seconds
{
	if (!_isPreparingBackward)
	{
		_isPreparingBackward = YES;
		[self performSelector:@selector(performBackward) withObject:nil afterDelay:seconds];
	}
}

- (void) cancelPerformBackward
{
	_isPreparingBackward = NO;
	[NSObject cancelPreviousPerformRequestsWithTarget:self];
}

- (void) executeInView:(UIView *)container fromImg:(UIImage *)fromImg toImg:(UIImage *)toImg
{
	[NSException raise:NPP_ERROR_INVALID format:NPP_ANI_ERROR_ABSTRACT, [self class]];
}

+ (void) defineTransitionDuration:(CGFloat)duration
{
	_nppDefaultDuration = duration;
}

+ (void) defineTransitionDirection:(NPPDirection)direction
{
	_nppDefaultDirection = direction;
}

+ (void) defineModalTransitionCategoryUsage:(BOOL)useCategory
{
	// The swizzle routine works as a "toggle" change.
	// This is a small lock to deal with "toggle".
	static BOOL _isUsingCategory = NO;
	if (useCategory != _isUsingCategory)
	{
		_isUsingCategory = useCategory;
	}
	// In case the swizzle option is already made, avoid "toggling".
	else
	{
		return;
	}
	
	Class aClass = [UIViewController class];
	SEL oldPresentSel, newPresentSel;
	SEL oldDismissSel, newDismissSel;
	
	// Backward compatibility for iOS 4.
	if (nppDeviceSystemVersion() < NPP_IOS_5_0)
	{
		oldPresentSel = @selector(presentModalViewController:animated:);
		newPresentSel = @selector(nppSwPresentModalViewController:animated:);
		oldDismissSel = @selector(dismissModalViewControllerAnimated:);
		newDismissSel = @selector(nppSwDismissModalViewControllerAnimated:);
	}
	// Current API. Even the old methods will call the new API if it's available.
	else
	{
		oldPresentSel = @selector(presentViewController:animated:completion:);
		newPresentSel = @selector(nppSwPresentViewController:animated:completion:);
		oldDismissSel = @selector(dismissViewControllerAnimated:completion:);
		newDismissSel = @selector(nppSwDismissViewControllerAnimated:completion:);
	}
	
	// Turn ON or OFF the category feature.
	if (_isUsingCategory)
	{
		nppSwizzle(aClass, oldPresentSel, newPresentSel);
		nppSwizzle(aClass, oldDismissSel, newDismissSel);
	}
	else
	{
		nppSwizzle(aClass, newPresentSel, oldPresentSel);
		nppSwizzle(aClass, newDismissSel, oldDismissSel);
	}
}

+ (void) defineModalTransitionClass:(Class)aClass direction:(NPPDirection)direction
{
	if (![aClass isSubclassOfClass:[NPPTransition class]])
	{
		[NSException raise:NPP_ERROR_INVALID format:NPP_ANI_ERROR_SUBLCASS, aClass];
		return;
	}
	
	_nppDefaultModalClass = aClass;
	_nppDefaultModalDirection = direction;
}

+ (void) definePushTransitionCategoryUsage:(BOOL)useCategory
{
	static BOOL _isUsingCategory = NO;
	
	// The swizzle routine works as a "toggle" change.
	// This is a small lock to deal with "toggle".
	if (useCategory != _isUsingCategory)
	{
		_isUsingCategory = useCategory;
	}
	// In case the swizzle option is already made, avoid "toggling".
	else
	{
		return;
	}
	
	Class aClass = [UINavigationController class];
	SEL oldPushSel, newPushSel;
	SEL oldPopSel, newPopSel;
	SEL oldPopToSel, newPopToSel;
	SEL oldPopRootSel, newPopRootSel;
	
	oldPushSel = @selector(pushViewController:animated:);
	newPushSel = @selector(nppSwPushViewController:animated:);
	oldPopSel = @selector(popViewControllerAnimated:);
	newPopSel = @selector(nppSwPopViewControllerAnimated:);
	oldPopToSel = @selector(popToViewController:animated:);
	newPopToSel = @selector(nppSwPopToViewController:animated:);
	oldPopRootSel = @selector(popToRootViewControllerAnimated:);
	newPopRootSel = @selector(nppSwPopToRootViewControllerAnimated:);
	
	// Turn ON or OFF the category feature.
	if (_isUsingCategory)
	{
		nppSwizzle(aClass, oldPushSel, newPushSel);
		nppSwizzle(aClass, oldPopSel, newPopSel);
		nppSwizzle(aClass, oldPopToSel, newPopToSel);
		nppSwizzle(aClass, oldPopRootSel, newPopRootSel);
	}
	else
	{
		nppSwizzle(aClass, newPushSel, oldPushSel);
		nppSwizzle(aClass, newPopSel, oldPopSel);
		nppSwizzle(aClass, newPopToSel, oldPopToSel);
		nppSwizzle(aClass, newPopRootSel, oldPopRootSel);
	}
}

+ (void) definePushTransitionClass:(Class)aClass direction:(NPPDirection)direction
{
	if (![aClass isSubclassOfClass:[NPPTransition class]])
	{
		[NSException raise:NPP_ERROR_INVALID format:NPP_ANI_ERROR_SUBLCASS, aClass];
		return;
	}
	
	_nppDefaultPushClass = aClass;
	_nppDefaultPushDirection = direction;
}

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (void) perform
{
	_isAnimating = YES;
	
	BOOL isBackward = _isBackward;
	UIView *currentView = (isBackward) ? _toView : _fromView;
	UIView *finalView = (isBackward) ? _fromView : _toView;
	UIView *superView = [currentView superview];
	UIView *container = nil;
	UIImage *fromImg = nil;
	UIImage *toImg = nil;
	CGRect fromFrame = _fromView.frame;
	CGRect toFrame = _toView.frame;
	CGRect fromBounds = _fromView.bounds;
	CGRect toBounds = _toView.bounds;
	CGRect fullRect = toFrame;
	CGFloat distW, distH;
	CGPoint center;
	BOOL isDifferent = NO;
	
	// The from view must be a subview of any other canvas.
	if (superView == nil)
	{
		[NSException raise:NPP_ERROR_INVALID format:NPP_ANI_ERROR_SUPERVIEW];
		return;
	}
	
	//*************************
	//	Rect calculations
	//*************************
	
	fromBounds.origin = CGPointZero;
	toBounds.origin = CGPointZero;
	
	// Override mode just animate the section that corresponds to the destination view.
	if (_mode == NPPTransitionModeOverride)
	{
		fromBounds.origin.x = toFrame.origin.x - fromFrame.origin.x;
		fromBounds.origin.y = toFrame.origin.y - fromFrame.origin.y;
		fromBounds.size = toFrame.size;
		
		center = (CGPoint){CGRectGetMidX(fullRect), CGRectGetMidY(fullRect)};
	}
	// Navigation mode has a special treatment to deal with both views. It takes use of the minimum size.
	else if (_mode == NPPTransitionModeNavigate)
	{
		// Calculating the snapshot frame to take from the bigger view.
		if (toBounds.size.height > fromBounds.size.height)
		{
			toBounds.origin.y = toBounds.size.height - fromBounds.size.height;
			toBounds.size.height -= toBounds.origin.y;
			isDifferent = YES;
		}
		else if (toBounds.size.height < fromBounds.size.height)
		{
			fromBounds.origin.y = fromBounds.size.height - toBounds.size.height;
			fromBounds.size.height -= fromBounds.origin.y;
			isDifferent = YES;
		}
		
		// The full rect is changed when the views doesn't have the same size.
		fullRect = (isDifferent) ? [superView convertRect:finalView.frame fromView:currentView] : fullRect;
		
		// Calculates and transforms the full rect considering the superview.
		center = (CGPoint){CGRectGetMidX(fullRect), CGRectGetMidY(fullRect)};
		if ([superView isKindOfClass:[UIWindow class]])
		{
			fullRect = [currentView convertRect:fullRect fromView:nil];
		}
	}
	// Any other mode will animate the entire frame, calculated by the needed size of both views.
	else
	{
		// It calculates the highest distance that a view can reach.
		distW = MAX(fromFrame.origin.x + fromFrame.size.width, toFrame.origin.x + toFrame.size.width);
		distH = MAX(fromFrame.origin.y + fromFrame.size.height, toFrame.origin.y + toFrame.size.height);
		
		// It calculates the minimum position a view can reach.
		fullRect.origin.x = MIN(fromFrame.origin.x, toFrame.origin.x);
		fullRect.origin.y = MIN(fromFrame.origin.y, toFrame.origin.y);
		
		// It calculates the final size.
		fullRect.size = (CGSize){ distW - fullRect.origin.x, distH - fullRect.origin.y };
		
		// It recalculates the bounds for both views' snapshots.
		fromBounds.origin.x = fullRect.origin.x - fromFrame.origin.x;
		fromBounds.origin.y = fullRect.origin.y - fromFrame.origin.y;
		fromBounds.size = fullRect.size;
		
		toBounds.origin.x = fullRect.origin.x - toFrame.origin.x;
		toBounds.origin.y = fullRect.origin.y - toFrame.origin.y;
		toBounds.size = fullRect.size;
		
		center = (CGPoint){CGRectGetMidX(fullRect), CGRectGetMidY(fullRect)};
	}
	
	//*************************
	//	Canvas Settings
	//*************************
	
	// None of the views can be hidden before the snapshot.
	currentView.hidden = NO;
	finalView.hidden = NO;
	
	// Takes a snapshot from the current state of both views.
	// The snapshot automatically fills with alpha any pixel outside the view's frame.
	fromImg = [currentView snapshotInRect:(isBackward) ? toBounds : fromBounds];
	toImg = [finalView snapshotInRect:(isBackward) ? fromBounds : toBounds];
	
	// Creating the container view. Here is where the animation happens.
	// The container view will be placed on top of both views.
	container = [[UIView alloc] initWithFrame:fullRect];
	container.backgroundColor = [UIColor clearColor];
	container.opaque = NO;
	container.clipsToBounds = YES;
	container.transform = currentView.transform;
	[superView insertSubview:container atIndex:nppTransitionGetTopIndex(currentView, finalView)];
	[container.layer setPosition:center];
	
	// Once the conaiter is ready, prepares the views to start animating.
	[self changeViewsState:YES backward:isBackward container:container];
	
	//*************************
	//	Animations
	//*************************
	
	// Creating the transaction call back and setting global parameters.
	[CATransaction begin];
	[CATransaction setValue:[NSNumber numberWithFloat:_duration] forKey:kCATransactionAnimationDuration];
	[CATransaction setCompletionBlock:^(void)
	 {
		 _isAnimating = NO;
		 
		 // The 'self' and 'container' here are automatically retained by the block system, which creates
		 // a strong link until the animation is done, making sure the instances will not be deallocated.
		 
		 // Prepares the views to finish animating.
		 [self changeViewsState:NO backward:isBackward container:container];
		 [container removeFromSuperview];
		 
		 // Call back block.
		 nppBlock(_completionBlock);
	 }];
	
	// It makes the specific animation, handling with 'direction'.
	[self executeInView:container fromImg:fromImg toImg:toImg];
	nppRelease(container);
	
	[CATransaction commit];
}

- (NSString *) description
{
	NSString *string = [NSString stringWithFormat:@"%@\n\
						From: %@\n\
						To: %@\n\
						Duration: %f\n\
						Direction: %i\n\
						Mode: %i\n\
						Backward: %i",
						[super description],
						_fromView,
						_toView,
						_duration,
						_direction,
						_mode,
						_isBackward];
	
	return string;
}

- (void) dealloc
{
	nppRelease(_fromView);
	nppRelease(_toView);
	
#ifndef NPP_ARC
	[super dealloc];
#endif
}

@end

#pragma mark -
#pragma mark UIViewController Category
#pragma mark -
//**********************************************************************************************************
//
//	UIViewController Categories
//
//**********************************************************************************************************

@implementation UIViewController(NPPTransition)

#pragma mark -
#pragma mark Properties
//**************************************************
//	Properties
//**************************************************

#pragma mark -
#pragma mark Constructors
//**************************************************
//	Constructors
//**************************************************

#pragma mark -
#pragma mark Private Methods
//**************************************************
//	Private Methods
//**************************************************

- (void) nppSwPresentViewController:(UIViewController *)viewController
						   animated:(BOOL)flag
						 completion:(void (^)(void))completion
{
	NPPTransition *transition = [_nppDefaultModalClass transitionWithcompletion:completion];
	transition.direction = _nppDefaultModalDirection;
	[self nppPresentViewController:viewController animated:flag transition:transition];
}

- (void) nppSwDismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion
{
	NPPTransition *transition = [_nppDefaultModalClass transitionWithcompletion:completion];
	transition.direction = _nppDefaultModalDirection;
	[self nppDismissViewControllerAnimated:flag transition:transition];
}

- (void) nppSwPresentModalViewController:(UIViewController *)viewController animated:(BOOL)animated
{
	[self nppPresentViewController:viewController animated:animated transition:nil];
}

- (void) nppSwDismissModalViewControllerAnimated:(BOOL)animated
{
	[self nppDismissViewControllerAnimated:animated transition:nil];
}

#pragma mark -
#pragma mark Self Public Methods
//**************************************************
//	Self Public Methods
//**************************************************

- (void) nppPresentViewController:(UIViewController *)controller
						 animated:(BOOL)flag
					   transition:(NPPTransition *)transition
{
	NPPBlockVoid completion = ^(void)
	{
		// Backward compatibility for iOS 4.
		if (nppDeviceSystemVersion() < NPP_IOS_5_0)
		{
			[self nppSwPresentModalViewController:controller animated:NO];
		}
		// Current API.
		else
		{
			[self nppSwPresentViewController:controller animated:NO completion:nil];
		}
	};
	
	if (flag)
	{
		UIViewController *navigation = [self navigationController];
		UIViewController *from = (navigation != nil) ? navigation : self;
		
		[NPPTransition defineViewFramesFrom:from to:controller];
		
		// Creating a new transition if it's not already set.
		if (transition == nil)
		{
			transition = [_nppDefaultModalClass transitionWithcompletion:completion];
			transition.direction = _nppDefaultModalDirection;
		}
		
		// Fixed animation parameters. These parameters will be ignored from the original transition.
		transition.fromView = from.view;
		transition.toView = controller.view;
		transition.completionBlock = completion;
		transition.backward = NO;
		
		// Animating.
		[transition perform];
	}
	else
	{
		nppBlock(completion);
	}
}

- (void) nppDismissViewControllerAnimated:(BOOL)flag transition:(NPPTransition *)transition
{
	NPPBlockVoid completion = ^(void)
	{
		// Backward compatibility for iOS 4.
		if (nppDeviceSystemVersion() < NPP_IOS_5_0)
		{
			[self nppSwDismissModalViewControllerAnimated:NO];
		}
		// Current API.
		else
		{
			[self nppSwDismissViewControllerAnimated:NO completion:nil];
		}
	};
	
	if (flag)
	{
		UIViewController *from = [self presentedViewController];
		
		[NPPTransition defineViewFramesFrom:from to:self];
		
		// Creating a new transition if it's not already set.
		if (transition == nil)
		{
			transition = [_nppDefaultModalClass transitionWithcompletion:completion];
			transition.direction = _nppDefaultModalDirection;
		}
		
		// Fixed animation parameters. These parameters will be ignored from the original transition.
		transition.fromView = self.view;
		transition.toView = from.view;
		transition.completionBlock = completion;
		transition.backward = YES;
		
		// Animating.
		[transition perform];
	}
	else
	{
		nppBlock(completion);
	}
}

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

+ (void) load
{
	Class aClass = NSClassFromString(@"NPPTransitionCube");
	[NPPTransition defineModalTransitionCategoryUsage:YES];
	[NPPTransition defineModalTransitionClass:aClass direction:NPPDirectionUp];
}

@end

#pragma mark -
#pragma mark UIViewController Category
#pragma mark -
//**********************************************************************************************************
//
//	UIViewController Categories
//
//**********************************************************************************************************

@implementation UINavigationController(NPPTransition)

#pragma mark -
#pragma mark Properties
//**************************************************
//	Properties
//**************************************************

#pragma mark -
#pragma mark Constructors
//**************************************************
//	Constructors
//**************************************************

#pragma mark -
#pragma mark Private Methods
//**************************************************
//	Private Methods
//**************************************************

- (void) nppSwPushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
	[self nppPushViewController:viewController animated:animated transition:nil];
}

- (UIViewController *) nppSwPopViewControllerAnimated:(BOOL)animated
{
	return [self nppPopViewControllerAnimated:animated transition:nil];
}

- (NSArray *) nppSwPopToViewController:(UIViewController *)viewController animated:(BOOL)animated
{
	return [self nppPopToViewController:viewController animated:animated transition:nil];
}

- (NSArray *) nppSwPopToRootViewControllerAnimated:(BOOL)animated
{
	return [self nppPopToRootViewControllerAnimated:animated transition:nil];
}

#pragma mark -
#pragma mark Self Public Methods
//**************************************************
//	Self Public Methods
//**************************************************

- (void) nppPushViewController:(UIViewController *)controller
					  animated:(BOOL)flag
					transition:(NPPTransition *)transition
{
	NPPBlockVoid completion = ^(void)
	{
		[self nppSwPushViewController:controller animated:NO];
	};
	
	UIViewController *visibleViewController = self.visibleViewController;
	
	if (flag && visibleViewController != nil)
	{
		[NPPTransition defineViewFramesFrom:visibleViewController to:controller];
		
		// Creating a new transition if it's not already set.
		if (transition == nil)
		{
			transition = [_nppDefaultPushClass transitionWithcompletion:completion];
			transition.direction = _nppDefaultPushDirection;
		}
		
		// Fixed animation parameters. These parameters will be ignored from the original transition.
		transition.fromView = visibleViewController.view;
		transition.toView = controller.view;
		transition.completionBlock = completion;
		transition.backward = NO;
		
		// Animating.
		[transition perform];
	}
	else
	{
		nppBlock(completion);
	}
}

- (UIViewController *) nppPopViewControllerAnimated:(BOOL)animated transition:(NPPTransition *)transition
{
	NSArray *controllers = [self viewControllers];
	NSUInteger count = [controllers count];
	
	return [[self nppPopToViewController:(count > 1) ? [controllers objectAtIndex:count - 2] : nil
								animated:animated
							  transition:transition] objectAtIndex:0];
}

- (NSArray *) nppPopToRootViewControllerAnimated:(BOOL)animated transition:(NPPTransition *)transition
{
	return [self nppPopToViewController:[[self viewControllers] objectAtIndex:0]
							   animated:animated
							 transition:transition];
}

- (NSArray *) nppPopToViewController:(UIViewController *)controller
							animated:(BOOL)flag
						  transition:(NPPTransition *)transition
{
	NPPBlockVoid completion = ^(void)
	{
		[self nppSwPopToViewController:controller animated:NO];
	};
	
	UIViewController *visibleViewController = [self visibleViewController];
	BOOL isPresented = [controller isEqual:visibleViewController];
	NSArray *controllers = [self viewControllers];
	NSUInteger index = [controllers indexOfObject:controller];
	NSRange range = NSMakeRange(0, 0);
	
	if (index != NSNotFound && !isPresented)
	{
		range = NSMakeRange(index + 1, [controllers count] - index - 1);
		
		if (flag)
		{
			[NPPTransition defineViewFramesFrom:visibleViewController to:controller];
			
			// Creating a new transition if it's not already set.
			if (transition == nil)
			{
				transition = [_nppDefaultPushClass transitionWithcompletion:completion];
				transition.direction = _nppDefaultPushDirection;
			}
			
			// Fixed animation parameters. These parameters will be ignored from the original transition.
			transition.fromView = controller.view;
			transition.toView = visibleViewController.view;
			transition.completionBlock = completion;
			transition.backward = YES;
			
			// Animating.
			[transition perform];
		}
		else
		{
			nppBlock(completion);
		}
	}
	
	return (index != NSNotFound && !isPresented) ? [controllers subarrayWithRange:range] : nil;
}

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

+ (void) load
{
	Class aClass = NSClassFromString(@"NPPTransitionScale");
	[NPPTransition definePushTransitionCategoryUsage:YES];
	[NPPTransition definePushTransitionClass:aClass direction:NPPDirectionLeft];
}

@end