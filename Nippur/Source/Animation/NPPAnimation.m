/*
 *	NPPAnimation.m
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 6/1/13.
 *	Copyright 2013 db-in. All rights reserved.
 */

#import "NPPAnimation.h"

#pragma mark -
#pragma mark Constants
#pragma mark -
//**********************************************************************************************************
//
//	Constants
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Interface
#pragma mark -
//**********************************************************************************************************
//
//	Private Interface
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Definitions
//**************************************************
//	Private Definitions
//**************************************************

#pragma mark -
#pragma mark Private Functions
//**************************************************
//	Private Functions
//**************************************************

static id nppAnimateEaseFunction(UIViewAnimationCurve ease)
{
	NSString *easeName = nil;
	
	switch (ease)
	{
		case UIViewAnimationCurveEaseOut:
			easeName = kCAMediaTimingFunctionEaseOut;
			break;
		case UIViewAnimationCurveEaseIn:
			easeName = kCAMediaTimingFunctionEaseIn;
			break;
		case UIViewAnimationCurveEaseInOut:
			easeName = kCAMediaTimingFunctionEaseInEaseOut;
			break;
		case UIViewAnimationCurveLinear:
			easeName = kCAMediaTimingFunctionLinear;
			break;
		default:
			easeName = kCAMediaTimingFunctionDefault;
			break;
	}
	
	return [CAMediaTimingFunction functionWithName:easeName];
}

#pragma mark -
#pragma mark Private Category
//**************************************************
//	Private Category
//**************************************************

#pragma mark -
#pragma mark Public Interface
#pragma mark -
//**********************************************************************************************************
//
//	Public Interface
//
//**********************************************************************************************************

void nppAnimateTo(CALayer *target, NSString *keyPath, CGFloat toValue)
{
	CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:keyPath];
	[animation setToValue:[NSNumber numberWithFloat:toValue]];
	[animation setFillMode:kCAFillModeForwards];
	[animation setRemovedOnCompletion:NO];
	[target addAnimation:animation forKey:nil];
}

void nppAnimateFrom(CALayer *target, NSString *keyPath, CGFloat fromValue)
{
	CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:keyPath];
	[animation setFromValue:[NSNumber numberWithFloat:fromValue]];
	[animation setFillMode:kCAFillModeBackwards];
	[animation setRemovedOnCompletion:NO];
	[target addAnimation:animation forKey:nil];
}

void nppAnimateFullTo(CALayer *target,
					  CGFloat duration,
					  CGFloat delay,
					  UIViewAnimationCurve ease,
					  NSString *keyPath,
					  CGFloat toValue)
{
	CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:keyPath];
	[animation setDuration:duration];
	[animation setBeginTime:CACurrentMediaTime() + delay];
	[animation setTimingFunction:nppAnimateEaseFunction(ease)];
	[animation setRemovedOnCompletion:NO];
	[animation setToValue:[NSNumber numberWithFloat:toValue]];
	[animation setFillMode:kCAFillModeForwards];
	[target addAnimation:animation forKey:nil];
}

void nppAnimateFullFrom(CALayer *target,
						CGFloat duration,
						CGFloat delay,
						UIViewAnimationCurve ease,
						NSString *keyPath,
						CGFloat fromValue)
{
	CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:keyPath];
	[animation setDuration:duration];
	[animation setBeginTime:CACurrentMediaTime() + delay];
	[animation setTimingFunction:nppAnimateEaseFunction(ease)];
	[animation setRemovedOnCompletion:NO];
	[animation setFromValue:[NSNumber numberWithFloat:fromValue]];
	[animation setFillMode:kCAFillModeBackwards];
	[target addAnimation:animation forKey:nil];
}

@implementation NPPAnimation

#pragma mark -
#pragma mark Properties
//**************************************************
//	Properties
//**************************************************

@dynamic duration, completionBlock;

- (CGFloat) duration { return _duration; }
- (void) setDuration:(CGFloat)value
{
	// Silently ignores any change while animating.
	if (!_isAnimating)
	{
		_duration = value;
	}
}

- (NPPBlockVoid) completionBlock { return _completionBlock; }
- (void) setCompletionBlock:(NPPBlockVoid)value
{
	// Silently ignores any change while animating.
	if (!_isAnimating && _completionBlock != value)
	{
		nppRelease(_completionBlock);
		_completionBlock = [value copy];
	}
}

#pragma mark -
#pragma mark Constructors
//**************************************************
//	Constructors
//**************************************************

- (id) initWithDuration:(CGFloat)duration completion:(NPPBlockVoid)block
{
	if ((self = [super init]))
	{
		self.duration = duration;
		self.completionBlock = block;
	}
	
	return self;
}

#pragma mark -
#pragma mark Private Methods
//**************************************************
//	Private Methods
//**************************************************

#pragma mark -
#pragma mark Self Public Methods
//**************************************************
//	Self Public Methods
//**************************************************

- (void) perform
{
	// Nothing to do here, just to override.
}

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (void) dealloc
{
	nppRelease(_completionBlock);
	
#ifndef NPP_ARC
	[super dealloc];
#endif
}

+ (BOOL) accessInstanceVariablesDirectly
{
	return NO;
}

@end
