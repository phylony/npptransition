/*
 *	NPPTransitionFold.m
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 6/1/13.
 *	Copyright 2013 db-in. All rights reserved.
 */

#import "NPPTransitionFold.h"

#pragma mark -
#pragma mark Constants
#pragma mark -
//**********************************************************************************************************
//
//	Constants
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Interface
#pragma mark -
//**********************************************************************************************************
//
//	Private Interface
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Definitions
//**************************************************
//	Private Definitions
//**************************************************

#pragma mark -
#pragma mark Private Functions
//**************************************************
//	Private Functions
//**************************************************

#pragma mark -
#pragma mark Private Category
//**************************************************
//	Private Category
//**************************************************

#pragma mark -
#pragma mark Public Interface
#pragma mark -
//**********************************************************************************************************
//
//	Public Interface
//
//**********************************************************************************************************

@implementation NPPTransitionFold

#pragma mark -
#pragma mark Properties
//**************************************************
//	Properties
//**************************************************

#pragma mark -
#pragma mark Constructors
//**************************************************
//	Constructors
//**************************************************

#pragma mark -
#pragma mark Private Methods
//**************************************************
//	Private Methods
//**************************************************

#pragma mark -
#pragma mark Self Public Methods
//**************************************************
//	Self Public Methods
//**************************************************

- (void) executeInView:(UIView *)container fromImg:(UIImage *)fromImg toImg:(UIImage *)toImg
{
	// Definitions.
	CGSize outSize = fromImg.size;
	CGSize inSize = toImg.size;
	CALayer *holderLayer = container.layer;
	
	// Colors.
	UIColor *blackColor = [UIColor blackColor];
	UIColor *clearColor = [UIColor clearColor];
	
	// From layer (outcoming layer).
	CALayer *outLayer = [CALayer layer];
	outLayer.frame = (CGRect){ CGPointZero, outSize };
	[outLayer setContents:(id)[fromImg CGImage]];
	
	// To layer (incoming layer).
	CALayer *inLayer = [CALayer layer];
	inLayer.frame = (CGRect){ CGPointZero, inSize };
	[inLayer setContents:(id)[toImg CGImage]];
	
	// Shadow layer.
	CAGradientLayer *outShadow = [CAGradientLayer layer];
	outShadow.opacity = 0.75f;
	outShadow.colors = [NSArray arrayWithObjects:(id)[blackColor CGColor], (id)[clearColor CGColor], nil];
	
	// Fade requires black background.
	container.backgroundColor = blackColor;
	
	NSString *keyMove = nil;
	NSString *keyRotation = nil;
	NSString *keyAlpha = @"opacity";
	CGFloat outRotation;
	CGPoint outPivot, inPivot;
	CGFloat outMove, inMove;
	
	// Core Animation perspective camera simulation.
	CATransform3D transform = CATransform3DIdentity;
	transform.m34 = -1.0 / (outSize.height * 2.0f);
	container.layer.sublayerTransform = transform;
	
	switch (_direction)
	{
		case NPPDirectionUp:
			keyMove = @"transform.translation.y";
			keyRotation = @"transform.rotation.x";
			outRotation = 90.0f;
			outMove = -outSize.height;
			inMove = inSize.height;
			outPivot = (CGPoint){ 0.5f, 1.0f };
			inPivot = (CGPoint){ 0.5f, 0.0f };
			outShadow.frame = CGRectMake(0.0f, 0.0f, outSize.width, outSize.height * 0.5f);
			break;
		case NPPDirectionDown:
			keyMove = @"transform.translation.y";
			keyRotation = @"transform.rotation.x";
			outRotation = -90.0f;
			outMove = outSize.height;
			inMove = -inSize.height;
			outPivot = (CGPoint){ 0.5f, 0.0f };
			inPivot = (CGPoint){ 0.5f, 1.0f };
			outShadow.frame = CGRectMake(0.0f, 0.0f, outSize.width, outSize.height * 0.5f);
			break;
		case NPPDirectionRight:
			keyMove = @"transform.translation.x";
			keyRotation = @"transform.rotation.y";
			outRotation = 90.0f;
			outMove = outSize.width;
			inMove = -inSize.width;
			outPivot = (CGPoint){ 0.0f, 0.5f };
			inPivot = (CGPoint){ 1.0f, 0.5f };
			outShadow.frame = CGRectMake(0.0f, 0.0f, outSize.width * 0.5f, outSize.height);
			break;
		case NPPDirectionLeft:
		default:
			keyMove = @"transform.translation.x";
			keyRotation = @"transform.rotation.y";
			outRotation = -90.0f;
			outMove = -outSize.width;
			inMove = inSize.width;
			outPivot = (CGPoint){ 1.0f, 0.5f };
			inPivot = (CGPoint){ 0.0f, 0.5f };
			outShadow.frame = CGRectMake(0.0f, 0.0f, outSize.width * 0.5f, outSize.height);
			break;
	}
	
	outRotation = NPPDegreesToRadians(outRotation);
	outShadow.startPoint = inPivot;
	outShadow.endPoint = outPivot;
	
	if (_isBackward)
	{
		[holderLayer addSublayer:inLayer];
		[holderLayer addSublayer:outLayer];
		[inLayer addSublayer:outShadow];
		
		outLayer.pivot = inPivot;
		inLayer.pivot = outPivot;
		
		nppAnimateFrom(inLayer, keyAlpha, 0.5f);
		nppAnimateFrom(inLayer, keyRotation, outRotation);
		nppAnimateFrom(inLayer, keyMove, outMove);
		nppAnimateTo(outLayer, keyMove, inMove);
		nppAnimateTo(outShadow, keyAlpha, 0.0f);
	}
	else
	{
		[holderLayer addSublayer:outLayer];
		[holderLayer addSublayer:inLayer];
		[outLayer addSublayer:outShadow];
		
		outLayer.pivot = outPivot;
		inLayer.pivot = inPivot;
		
		nppAnimateTo(outLayer, keyAlpha, 0.5f);
		nppAnimateTo(outLayer, keyRotation, outRotation);
		nppAnimateTo(outLayer, keyMove, outMove);
		nppAnimateFrom(inLayer, keyMove, inMove);
		nppAnimateFrom(outShadow, keyAlpha, 0.0f);
	}
}

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

@end

#pragma mark -
#pragma mark NPPTransitionFoldSeguePush
#pragma mark -
//**********************************************************************************************************
//
//	NPPTransitionFoldSeguePush
//
//**********************************************************************************************************

@implementation NPPTransitionFoldSeguePush

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (void) perform
{
    UIViewController *src = self.sourceViewController;
    UIViewController *dest = self.destinationViewController;
	UINavigationController *navigation = src.navigationController;
	NPPTransition *transition = [NPPTransitionFold transitionWithcompletion:nil];
	[navigation nppPushViewController:dest animated:YES transition:transition];
}

@end

#pragma mark -
#pragma mark NPPTransitionFoldSegueModal
#pragma mark -
//**********************************************************************************************************
//
//	NPPTransitionFoldSegueModal
//
//**********************************************************************************************************

@implementation NPPTransitionFoldSegueModal

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (void) perform
{
    UIViewController *src = self.sourceViewController;
    UIViewController *dest = self.destinationViewController;
	NPPTransition *transition = [NPPTransitionFold transitionWithcompletion:nil];
	[src nppPresentViewController:dest animated:YES transition:transition];
}

@end